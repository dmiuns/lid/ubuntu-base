#!/bin/sh

sudo apt install -yq ansible

sudo ansible-galaxy collection install community.general

TARGET="$1"
if [ -z "$1" ]; then
	TARGET="metal"
fi

case "$TARGET" in
	vm)
		sudo ansible-playbook -i localhost, -c local -t base,devel main.yml
		;;
	metal)
		sudo ansible-playbook -i localhost, -c local main.yml
		;;
	*)
		echo Invalid target
		;;
esac
